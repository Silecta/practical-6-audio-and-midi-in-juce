/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback,
                        public Slider::Listener,
                        public Button::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized();
    void handleIncomingMidiMessage(MidiInput* Source, const MidiMessage& message) override;
    void setText (const String &newText, NotificationType notification);
    void buttonClicked(Button* button) override;
    void sliderValueChanged (Slider* slider) override;

private:
        //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    
    AudioDeviceManager audioDeviceManager;
    Slider channelSlider;
    Slider numberSlider;
    Slider velocitySlider;
    ComboBox messageType;
    TextButton sendButton;
    Label channelLabel;
    Label numberLabel;
    Label velocityLabel;
    Label messageTypeLabel;
};


#endif  // MAINCOMPONENT_H_INCLUDED
