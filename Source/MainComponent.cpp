/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    
    messageTypeLabel.setText("Message Type", dontSendNotification);
    addAndMakeVisible(&messageTypeLabel);
    
    channelSlider.setSliderStyle(juce::Slider::IncDecButtons);
    channelSlider.setRange(1.0, 16.0, 1.0);
    addAndMakeVisible (&channelSlider);
    channelLabel.setText("Channel", dontSendNotification);
    addAndMakeVisible (&channelLabel);
    
    numberSlider.setSliderStyle(juce::Slider::IncDecButtons);
    numberSlider.setRange(1.0, 144.0, 1.0);
    addAndMakeVisible (&numberSlider);
    numberLabel.setText("Note Number", dontSendNotification);
    addAndMakeVisible (&numberLabel);
    
    velocitySlider.setSliderStyle(juce::Slider::IncDecButtons);
    velocitySlider.setRange(0.0, 127.0, 1.0);
    addAndMakeVisible (&velocitySlider);
    velocityLabel.setText("velocity", dontSendNotification);
    addAndMakeVisible (&velocityLabel);
    
    sendButton.setButtonText("Send MIDI Data");
    addAndMakeVisible(&sendButton);
    
    messageType.addItem("Note", 1);
    messageType.addItem("Control", 2);
    messageType.addItem("Program", 3);
    messageType.addItem("Velocity", 4);
    addAndMakeVisible(&messageType);
    

}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    messageType.setBounds(10, 50, 80, 40);
    messageTypeLabel.setBounds(10, 10, 80, 40);
    channelSlider.setBounds(100, 50, 80, 40);
    channelLabel.setBounds(100, 10, 80, 40);
    numberSlider.setBounds(200, 50, 80, 40);
    numberLabel.setBounds(200, 10, 80, 40);
    velocitySlider.setBounds(300, 50, 80, 40);
    velocityLabel.setBounds(300, 10, 80, 40);
    sendButton.setBounds(10, 100, getWidth() - 20, 40);
}

void MainComponent::handleIncomingMidiMessage(MidiInput* Source, const MidiMessage& message)
{
    DBG("MIDI Input Recieved");
}

void MainComponent::buttonClicked(Button* sendButton)
{
    DBG("sending MIDI Message!");
    
    MidiMessage message;
    
    message = MidiMessage::noteOn (channelSlider.getValue(), numberSlider.getValue(), (uint8)velocitySlider.getValue());
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    std::cout << "Channel:\t" << channelSlider.getValue();
    
    message.setVelocity(10);
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    DBG("Slider Value Changed!");
}
